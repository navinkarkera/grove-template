Contribution guidelines can be viewed at:
- https://gitlab.com/opencraft/dev/grove/-/blob/main/CONTRIBUTING.md
- https://grove.opencraft.com/contributing/
